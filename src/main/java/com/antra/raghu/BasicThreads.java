package com.antra.raghu;

class ProcessMessage extends Thread {
	
	@Override
	public void run() {
		System.out.println("Thread Started :" + getName() + " -- " + Thread.currentThread());
		for(int i=0;i<4;i++) {
			System.out.println("Thread : " + getName() + " = " + i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Thread Ended :" + getName());
	}
}
public class BasicThreads {

	public static void main(String[] args) {
		ProcessMessage p = new ProcessMessage();
		p.setName("WELCOME");
		p.start();
	}
}
