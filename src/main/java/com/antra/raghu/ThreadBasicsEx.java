package com.antra.raghu;

class First extends Thread {

	@Override
	public void run() {
		System.out.println("FROM THREAD " + getName());
	}
}

class Second implements Runnable {

	@Override
	public void run() {
		System.out.println("FROM THREAD " + Thread.currentThread().getName());
	}
}

public class ThreadBasicsEx {

	public static void main(String[] args) {

		System.out.println("FROM MAIN THREAD");
		//1. using thread class
		First f = new First();
		f.start();

		//2. using runnable interface
		Thread t2  = new Thread(new Second());
		t2.start();

		//3rd using Anonymous inner class
		new Thread(new Runnable() {
			public void run() {
				System.out.println("FROM THREAD " + Thread.currentThread().getName());
			}
		}).start();

		//4. using lambda 
		new Thread(()->{
			System.out.println("FROM THREAD " + Thread.currentThread().getName());
		}).start();
	}
}

