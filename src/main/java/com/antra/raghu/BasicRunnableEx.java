package com.antra.raghu;

class RunnableDemo implements Runnable {

	String name;
	Thread thread;
	
	public RunnableDemo(String name) {
		super();
		this.name = name;
		this.start();
	}
	
	@Override
	public void run() {
		System.out.println("Running " + name);
		try {
			for (int i = 4; i > 0; i--) {
				System.out.println("Thread :" + name + " = " + i);
				Thread.sleep(1000);
			}
		} catch (InterruptedException ae) {
			System.out.println("Thread " + name + " is interrupted");
			ae.printStackTrace();
		}
		System.out.println("Ended " + name);
	}
	public void start() {
		if(thread == null) {
			thread = new Thread(this, name);
			thread.start();
		}
	}
}

public class BasicRunnableEx {

	public static void main(String[] args) {
		RunnableDemo r1 = new RunnableDemo("WELCOME");
		RunnableDemo r2 = new RunnableDemo("ANTRA");
	}
}
