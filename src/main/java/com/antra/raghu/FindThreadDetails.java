package com.antra.raghu;

public class FindThreadDetails {

	public static void main(String[] args) {

		Thread t1 = Thread.currentThread();
		String name = t1.getName();
		System.out.println(name);
		int priority = t1.getPriority();
		System.out.println(priority);
		boolean daemon = t1.isDaemon();
		System.out.println(daemon);

	}

}
